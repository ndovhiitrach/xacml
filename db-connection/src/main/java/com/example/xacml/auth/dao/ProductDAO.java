package com.example.xacml.auth.dao;

import com.example.xacml.auth.model.Product;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDAO extends SimpleJpaRepository<Product, String>
    implements GenericDAO<Product> {
  @PersistenceContext
  private final EntityManager entityManager;

  public ProductDAO(EntityManager entityManager) {
    super(Product.class, entityManager);
    this.entityManager = entityManager;
  }

  @Override
  public EntityManager entityManager() {
    return this.entityManager;
  }
}
