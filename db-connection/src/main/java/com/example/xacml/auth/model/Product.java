package com.example.xacml.auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Product {
  @Id @Column private final String name;
  @Column private final float price;
  @Column private final String group;
  @Column private final ProductType type;
  @Column private final String category;
}
