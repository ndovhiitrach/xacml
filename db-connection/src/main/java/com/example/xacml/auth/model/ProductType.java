package com.example.xacml.auth.model;

public enum ProductType {
  EXCLUSIVE,
  ORDINARY
}
