package com.example.xacml.auth.dao;

import java.lang.reflect.ParameterizedType;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.transaction.annotation.Transactional;

public interface GenericDAO<T> {
  EntityManager entityManager();

  //  @Transactional
  //  public T findByName(String name) {
  //    CriteriaBuilder builder = entityManager.getCriteriaBuilder();
  //    CriteriaQuery<T> query = builder.createQuery(tClass);
  //    Root<T> root = query.from(tClass);
  //    return entityManager
  //        .createQuery(query.select(root).where(builder.equal(root.get("name"), name)))
  //        .getSingleResult();
  //  }

  //  @Transactional
  //  public List<T> findAll() {
  //    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
  //    CriteriaQuery<T> query = criteriaBuilder.createQuery(tClass);
  //    Root<T> root = query.from(tClass);
  //    return entityManager.createQuery(query.select(root)).getResultList();
  //  }
  @SuppressWarnings("unchecked")
  default Object getAttributeByName(String attributeName, String customerName) {
    Class<T> tClass =
        (Class<T>)
            ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    CriteriaBuilder criteriaBuilder = entityManager().getCriteriaBuilder();
    CriteriaQuery<Object> query = criteriaBuilder.createQuery();
    Root<T> root = query.from(tClass);
    return entityManager()
        .createQuery(
            query
                .select(root.get(attributeName))
                .where(criteriaBuilder.equal(root.get("name"), customerName)))
        .getSingleResult();
  }
}
