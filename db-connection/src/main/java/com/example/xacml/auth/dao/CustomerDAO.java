package com.example.xacml.auth.dao;

import com.example.xacml.auth.model.Customer;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDAO extends SimpleJpaRepository<Customer, String>
    implements GenericDAO<Customer> {

  @PersistenceContext private final EntityManager entityManager;

  public CustomerDAO(EntityManager entityManager) {
    super(Customer.class, entityManager);
    this.entityManager = entityManager;
  }

  @Override
  public EntityManager entityManager() {
    return this.entityManager;
  }
}
