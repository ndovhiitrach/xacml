package com.example.xacml.auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Customer {
  @Id @Column private final String name;
  @Column private final String password;
  @Column private final int age;
  @Column private final ClientType type;
}
