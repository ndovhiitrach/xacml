package com.example.xacml.auth.product;

import com.example.xacml.auth.type.RequestData;
import com.example.xacml.auth.type.ResponseData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class StoreController {

  @PostMapping("/product")
  public ResponseData buy(@RequestBody RequestData requestData) {
    return new ResponseData();
  }
}
