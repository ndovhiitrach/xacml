package com.example.xacml.auth.type;

import lombok.Data;

@Data
public class RequestData {
  private final String userName;
  private final String productName;
  private final int productNumber;
}
