package com.example.xacml.auth.util;

import com.example.xacml.auth.api.dto.RequestData;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import org.apache.commons.io.IOUtils;

public class RequestHelper {

  private RequestHelper() {}

  public static String toXACMLRequest(RequestData requestData) throws IOException {
    InputStream resourceAsStream =
        RequestHelper.class
            .getClassLoader()
            .getResourceAsStream("xacml-auth/src/main/resources/request/request.xml");
    String template = IOUtils.toString(Objects.requireNonNull(resourceAsStream), "UTF-8");
    return String.format(template, requestData.getUserName(), requestData.getProductName());
  }
}
