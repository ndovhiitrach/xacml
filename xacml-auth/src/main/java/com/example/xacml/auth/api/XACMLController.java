package com.example.xacml.auth.api;

import com.example.xacml.auth.api.dto.RequestData;
import com.example.xacml.auth.api.dto.ResponseData;
import com.example.xacml.auth.util.RequestHelper;
import com.example.xacml.auth.util.ResponseHelper;
import java.io.IOException;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.wso2.balana.PDP;
import org.wso2.balana.ParsingException;
import org.wso2.balana.ctx.AbstractResult;
import org.wso2.balana.ctx.ResponseCtx;

@Controller
public class XACMLController {
  private static final Logger LOG = LoggerFactory.getLogger(XACMLController.class);
  private final PDP pdp;

  public XACMLController(PDP pdp) {
    this.pdp = pdp;
  }

  @PostMapping("/evaluate")
  public ResponseEntity<ResponseData> evaluate(@RequestBody RequestData requestData) {
    ResponseData responseData =
        new ResponseData(
            "Deny", requestData.getUserName() + "\" is NOT authorized to perform this purchase");

    try {
      String xacmlResponse = pdp.evaluate(RequestHelper.toXACMLRequest(requestData));
      ResponseCtx responseCtx =
          ResponseCtx.getInstance(
              Objects.requireNonNull(ResponseHelper.getXacmlResponse(xacmlResponse)));
      AbstractResult result = responseCtx.getResults().iterator().next();
      responseData =
          new ResponseData("Permit", ResponseHelper.generateRespomseMessage(requestData, result));
    } catch (IOException | ParsingException e) {
      LOG.error(e.getMessage());
    }
    return ResponseEntity.ok(responseData);
  }
}
