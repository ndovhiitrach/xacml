package com.example.xacml.auth.api.dto;

import lombok.Data;

@Data
public class ResponseData {
  private final String status;
  private final String message;
}
