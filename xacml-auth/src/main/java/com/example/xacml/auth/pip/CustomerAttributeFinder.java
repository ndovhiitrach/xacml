package com.example.xacml.auth.pip;

import com.example.xacml.auth.dao.CustomerDAO;
import com.example.xacml.auth.model.Customer;
import com.example.xacml.auth.util.AttributeHelper;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.finder.AttributeFinderModule;

@Component
public class CustomerAttributeFinder extends AttributeFinderModule {
  private static final Logger LOG = LoggerFactory.getLogger(CustomerAttributeFinder.class);
  private final CustomerDAO customerDAO;

  private URI subjectId;

  public CustomerAttributeFinder(CustomerDAO customerDAO) {
    try {
      this.subjectId = new URI("urn:oasis:names:tc:xacml:1.0:subject:subject-id");
    } catch (URISyntaxException e) {
      LOG.error(e.getMessage());
    }
    this.customerDAO = customerDAO;
  }

  @Override
  public Set<String> getSupportedCategories() {
    Set<String> categories = new HashSet<String>();
    categories.add("urn:oasis:names:tc:xacml:1.0:subject-category:access-subject");
    return categories;
  }

  @Override
  public Set getSupportedIds() {
    Set<String> ids = new HashSet<>();
    customerDAO
        .findAll()
        .forEach(
            customer -> {
              Field[] fields =
                  ArrayUtils.addAll(Customer.class.getFields(), Customer.class.getDeclaredFields());
              Arrays.stream(fields)
                  .forEach(
                      field ->
                          ids.add(
                              "urn:oasis:names:tc:xacml:1.0:subject:attribute:" + field.getName()));
            });
    return ids;
  }

  @Override
  public boolean isDesignatorSupported() {
    return true;
  }

  @Override
  public EvaluationResult findAttribute(
      URI attributeType, URI attributeId, String issuer, URI category, EvaluationCtx context) {
    return AttributeHelper.findAttribute(
        attributeType, attributeId, issuer, category, context, customerDAO, subjectId);
  }
}
