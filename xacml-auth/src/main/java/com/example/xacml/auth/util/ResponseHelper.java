package com.example.xacml.auth.util;

import com.example.xacml.auth.api.dto.RequestData;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.wso2.balana.ctx.AbstractResult;
import org.wso2.balana.ctx.AttributeAssignment;
import org.wso2.balana.xacml3.Advice;

public class ResponseHelper {
  private static final Logger LOG = LoggerFactory.getLogger(ResponseHelper.class);

  private ResponseHelper() {}

  public static Element getXacmlResponse(String response) {

    ByteArrayInputStream inputStream;
    DocumentBuilderFactory dbf;
    Document doc;

    inputStream = new ByteArrayInputStream(response.getBytes());
    dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);

    try {
      doc = dbf.newDocumentBuilder().parse(inputStream);
    } catch (Exception e) {
      LOG.error("DOM of request element can not be created from String");
      return null;
    } finally {
      try {
        inputStream.close();
      } catch (IOException e) {
        LOG.error("Error in closing input stream of XACML response");
      }
    }
    return doc.getDocumentElement();
  }

  public static String generateRespomseMessage(RequestData requestData, AbstractResult result) {
    StringBuilder responseMessage = new StringBuilder(Strings.EMPTY);
    if (AbstractResult.DECISION_PERMIT == result.getDecision()) {
      responseMessage
          .append("\n")
          .append(requestData.getUserName())
          .append(" is authorized to perform this purchase\n\n");

    } else {
      responseMessage
          .append("\n")
          .append(requestData.getUserName())
          .append(" is NOT authorized to perform this purchase\n");

      List<Advice> advices = result.getAdvices();
      for (Advice advice : advices) {
        List<AttributeAssignment> assignments = advice.getAssignments();
        for (AttributeAssignment assignment : assignments) {
          responseMessage.append("Advice :  ").append(assignment.getContent()).append("\n\n");
        }
      }
    }
    return responseMessage.toString();
  }
}
