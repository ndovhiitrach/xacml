package com.example.xacml.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XACMLApplication {
  public static void main(String[] args) {
    SpringApplication.run(XACMLApplication.class, args);
  }
}
