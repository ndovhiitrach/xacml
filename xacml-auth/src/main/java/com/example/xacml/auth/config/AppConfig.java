package com.example.xacml.auth.config;

import com.example.xacml.auth.pip.CustomerAttributeFinder;
import com.example.xacml.auth.pip.ProductAttributeFinder;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.wso2.balana.Balana;
import org.wso2.balana.PDP;
import org.wso2.balana.PDPConfig;
import org.wso2.balana.finder.AttributeFinder;
import org.wso2.balana.finder.AttributeFinderModule;
import org.wso2.balana.finder.impl.FileBasedPolicyFinderModule;

@Configuration
public class AppConfig {
  @Bean
  public Balana balana() {
    String policyLocation = this.getClass().getClassLoader().getResource("policy").getFile();
    System.setProperty(FileBasedPolicyFinderModule.POLICY_DIR_PROPERTY, policyLocation);
    return Balana.getInstance();
  }

  @Bean
  public PDP pdp(
      CustomerAttributeFinder customerAttributeFinder,
      ProductAttributeFinder productAttributeFinder) {
    PDPConfig pdpConfig = balana().getPdpConfig();

    AttributeFinder attributeFinder = pdpConfig.getAttributeFinder();
    List<AttributeFinderModule> finderModules = attributeFinder.getModules();
    finderModules.add(customerAttributeFinder);
    finderModules.add(productAttributeFinder);
    attributeFinder.setModules(finderModules);

    return new PDP(new PDPConfig(attributeFinder, pdpConfig.getPolicyFinder(), null, true));
  }
}
