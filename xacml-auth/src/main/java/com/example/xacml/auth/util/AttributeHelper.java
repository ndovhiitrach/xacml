package com.example.xacml.auth.util;

import com.example.xacml.auth.dao.GenericDAO;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.wso2.balana.attr.AttributeValue;
import org.wso2.balana.attr.BagAttribute;
import org.wso2.balana.attr.DoubleAttribute;
import org.wso2.balana.attr.IntegerAttribute;
import org.wso2.balana.attr.StringAttribute;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;

public class AttributeHelper {

  private AttributeHelper() {}

  public static <T> EvaluationResult findAttribute(
      URI attributeType,
      URI attributeId,
      String issuer,
      URI category,
      EvaluationCtx context,
      GenericDAO<T> dao,
      URI defaultId) {
    String customerName = null;
    List<AttributeValue> attributeValues = new ArrayList<AttributeValue>();
    EvaluationResult result = context.getAttribute(attributeType, defaultId, issuer, category);

    if (Objects.nonNull(result)
        && Objects.nonNull(result.getAttributeValue())
        && result.getAttributeValue().isBag()) {

      BagAttribute bagAttribute = (BagAttribute) result.getAttributeValue();
      if (!bagAttribute.isEmpty()) {
        customerName = ((AttributeValue) bagAttribute.iterator().next()).encode();
      }
    }
    if (Objects.nonNull(customerName)) {
      String attributeName =
          attributeId.toString().substring(attributeId.toString().lastIndexOf("attribute:"));
      Object attribute = dao.getAttributeByName(attributeName, customerName);
      attributeValues.add(AttributeHelper.determineAttrType(attribute));
    }
    return new EvaluationResult(new BagAttribute(attributeType, attributeValues));
  }

  @SuppressWarnings("All")
  public static AttributeValue determineAttrType(Object attr) {
    AttributeValue result = null;
    if (attr instanceof String) {
      result = new StringAttribute(String.valueOf(attr));
    }
    if (attr instanceof Integer) {
      result = new IntegerAttribute((Long) attr);
    }
    if (attr instanceof Float || attr instanceof Double) {
      result = new DoubleAttribute((Double) attr);
    }
    return result;
  }
}
